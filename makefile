CPP=gcc    #Commande du compilateur
CFLAGS=-O3 -Wall -Wextra
LDFLAGS=$(shell echo `sdl-config --libs`)
LINKERFLAGS=$(shell echo `sdl-config --cflags`)
EXEC=physics
OBJ=obj
SRC=src

all: $(EXEC)

$(EXEC): $(OBJ)/$(EXEC).o
	$(CPP) $(CFLAGS) -o $@ $^ $(LDFLAGS) 

$(OBJ)/$(EXEC).o: $(SRC)/$(EXEC).c $(SRC)/$(EXEC).h
	$(CPP) $(CFLAGS) -c $< -o $@ $(LINKERFLAGS)



# ${EXEC}: ${OBJ}/${EXEC}.o
#	${CPP} $(CFLAGS) -o ${EXEC} ${OBJ}/${EXEC}.o ${LDFLAGS}

# ${OBJ}/${EXEC}.o: ${SRC}/${EXEC}.c
#	${CPP} $(CFLAGS) -o ${OBJ}/${EXEC}.o -c ${SRC}/${EXEC}.c


clean:
	rm -fr $(OBJ)/*.o

mrproper: clean
	rm -fr $(EXEC)
