#ifndef PHYSICS_H__
#define PHYSICS_H__

#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

void pause();

struct rectangle {
	SDL_Surface *surface;
	SDL_Rect position;
	Uint32 color;
};

typedef struct rectangle rect;
	

#endif /* !PHYSICS_H__ */
