#include "physics.h"

void pause()
{
	int continuer = 1;
	SDL_Event event;

	while (continuer){
		SDL_WaitEvent(&event);
		switch(event.type){
			case SDL_QUIT:
				continuer = 0;
				break;
		}
	}
}

void create_gradient(SDL_Surface** arr){
	int i;
	for(i = 0; i < 256; i++){
		arr[i] = SDL_CreateRGBSurface(SDL_HWSURFACE, 640, 1, 32, 0, 0, 0, 0);
	}
}

int main()
{
	int i;
	SDL_Surface *ecran;
	Uint32 bg;
	/* rect rectangle; */
	SDL_Surface* grad[256];
	SDL_Rect position;

	/* Initialisation of the SDL */
	if (SDL_Init(SDL_INIT_VIDEO) == -1)
	{
		fprintf(stderr, "Erreur d'init de la SDL : %s\n", SDL_GetError());

		exit(-1);
	}
	SDL_WM_SetCaption("Yeah first window", NULL);
	
	/* Assigning variables */
	ecran = SDL_SetVideoMode(640, 256, 32, SDL_HWSURFACE);
	if (ecran == NULL)
	{
		fprintf(stderr, "Impossible de charger le mode vidéo : %s\n", SDL_GetError());
		exit(-1);
	}

	/* Setup environnement */
	bg = SDL_MapRGB(ecran->format, 17, 206, 112);
	/*rectangle.surface = SDL_CreateRGBSurface(SDL_HWSURFACE, 100, 100, 32, 0, 0, 0, 0);
	rectangle.color = SDL_MapRGB(ecran->format, 255,255,255);
	rectangle.position.x = 50;
	rectangle.position.y = 50;*/
	create_gradient(grad);

	/* Filling everythinh */
	SDL_FillRect(ecran, NULL, bg);
	position.x = 0;
	for(i = 0; i < 256; i++){
		SDL_FillRect(grad[i], NULL, SDL_MapRGB(ecran->format, i, i, i));
		position.y = i;
		SDL_BlitSurface(grad[i], NULL, ecran, &position);
	}
	
	/* Stick to screen everything */
	
	/* Updates screen */
	SDL_Flip(ecran);
	
	/* Handling forever loop to wait for user to exit */
	pause();
	
	for(i =0; i < 256; i++)
		SDL_FreeSurface(grad[i]);
	SDL_Quit();

	return 0;
}
